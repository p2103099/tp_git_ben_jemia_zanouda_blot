package entree;

public interface Entree {
    
    public boolean recherche(String elem);
    
    public String toString(Presentation presentation, Sens sens);
}
