package entree;

public class Societe implements Entree {
    private String nomSociete;
    
    public Societe(String nomSociete) {
        this.nomSociete = nomSociete;
    }


    public boolean recherche(String elem) {
        if (this.nomSociete.contains(elem)) {
            return true;
        }
        return false;
    }


    public String getNomSociete() {
        return this.nomSociete;
    }

    @Override
    public String toString(Presentation presentation, Sens sens) {
        return this.nomSociete;
    }
}
