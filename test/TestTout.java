package test;

import carnet.Carnet;
import carnet.Ordre;
import entree.Entree;
import entree.Presentation;
import entree.Sens;

public class TestTout {
    public static void main(String[] args) {
        Carnet c = new Carnet();
        c.lectureFichier("test.txt");

        c.selection("Percev");
        c.selection("ames");
        c.selection("Weasley");

        c.affichageSelection(Ordre.DECROISSANT, Presentation.ABREGE, Sens.PRENOMS_NOM);
        System.out.println("\n");
        c.affichageSelection(Ordre.DECROISSANT, Presentation.ABREGE, Sens.NOM_PRENOMS);
        System.out.println("\n");
        c.affichageSelection(Ordre.DECROISSANT, Presentation.SIMPLE, Sens.PRENOMS_NOM);
        System.out.println("\n");
        c.affichageSelection(Ordre.DECROISSANT, Presentation.SIMPLE, Sens.NOM_PRENOMS);
        System.out.println("\n");
        c.affichageSelection(Ordre.DECROISSANT, Presentation.COMPLET, Sens.PRENOMS_NOM);
        System.out.println("\n");
        c.affichageSelection(Ordre.DECROISSANT, Presentation.COMPLET, Sens.NOM_PRENOMS);
        System.out.println("\n");
        c.affichageSelection(Ordre.CROISSANT, Presentation.ABREGE, Sens.PRENOMS_NOM);
        System.out.println("\n");
        c.affichageSelection(Ordre.CROISSANT, Presentation.ABREGE, Sens.NOM_PRENOMS);
        System.out.println("\n");
        c.affichageSelection(Ordre.CROISSANT, Presentation.SIMPLE, Sens.PRENOMS_NOM);
        System.out.println("\n");
        c.affichageSelection(Ordre.CROISSANT, Presentation.SIMPLE, Sens.NOM_PRENOMS);
        System.out.println("\n");
        c.affichageSelection(Ordre.CROISSANT, Presentation.COMPLET, Sens.PRENOMS_NOM);
        System.out.println("\n");
        c.affichageSelection(Ordre.CROISSANT, Presentation.COMPLET, Sens.NOM_PRENOMS);
    }
}
