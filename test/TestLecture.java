package test;

import carnet.*;
import entree.*;
import entree.Entree;

import java.util.ArrayList;


public class TestLecture {

    public static void main(String[] args) {
        Carnet c = new Carnet();
        c.lectureFichier("test.txt");
        System.out.println(c.nombreEntrees() + " doit etre egal à 4");
        c.afficher();
    }
    
}
