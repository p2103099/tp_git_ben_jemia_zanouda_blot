package test;

import carnet.Carnet;
import carnet.Ordre;
import entree.Entree;
import entree.Presentation;
import entree.Sens;

public class TestSelection {
    public static void main(String[] args) {
        Carnet c = new Carnet();
        c.lectureFichier("test.txt");

        c.selection("Ecole");
        c.selection("Dumb");
        c.selection("Pott");
        c.selection("Wea");

        
        c.affichageSelection(Ordre.CROISSANT, Presentation.SIMPLE, Sens.NOM_PRENOMS);
    }
}
