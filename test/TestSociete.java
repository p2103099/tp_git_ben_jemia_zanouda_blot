package test;

import entree.Societe;

public class TestSociete {
    public static void main(String[] args) {
        Societe samsung = new Societe("SAMSUNG ELECTRONICS");
        Societe bfm_tv = new Societe("BFM TV");

        System.out.println(samsung.recherche("SAMSUNG"));
        System.out.println(samsung.getNomSociete());
        System.out.println(bfm_tv.toString(null, null));
    }
}