package carnet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import entree.*;

public class Carnet {

    private ArrayList<Entree> entrees = new ArrayList<Entree>();
    private ArrayList<Entree> selectionnees = new ArrayList<Entree>();


    
    public void lectureFichier(String nomFichier) {

        String type;
        HashMap<Entree, Integer[]> personnes = new HashMap<>();

        // Read file line per line
        try {
            BufferedReader br = new BufferedReader(new FileReader(nomFichier));
            String line;

            while ((line = br.readLine()) != null) {
                // Split line into type and content
                String[] split = line.split(";");
                type = split[1];
                // Create new Entree
                Entree entree = null;
                int conjoint = -1;
                int societe = -1;
                switch (type) {
                    case "PERSONNE":
                        String[] prenoms = split[2].split(",");
                        String genre = split[3];
                        Genre g = null;
                        switch(genre) {
                            case "H":
                                g = Genre.HOMME;
                                break;
                            case "F":
                                g = Genre.FEMME;
                                break;
                        }
                        if(split[5].equals("")) {
                            conjoint = -1;
                        } else {
                            conjoint = Integer.parseInt(split[5]);
                        }
                        if(split[6].equals("")) {
                            societe = -1;
                        } else {
                            societe = Integer.parseInt(split[6]);
                        }
                        entree = new Personne(split[3], prenoms, split[7], g, null, null); // Object Personne et Société au lieu de String

                        break;
                    case "SOCIETE":
                        entree = new Societe(split[2]);
                        break;
                    default:
                        System.out.println("Type inconnu: " + type);
                        break;
                }
                // Add Entree to array
                if (entree != null) {
                    personnes.put(entree, new Integer[]{conjoint, societe});
                    entrees.add(entree);
                }
            }
            br.close();
        } catch (IOException e) {
            System.out.println("Erreur de lecture du fichier: " + e.getMessage());
        }
        for(Entree e : entrees) {
            if (e instanceof Personne) {
                Personne p = (Personne) e;
                if(personnes.get(p)[0] != -1) {
                    p.setConjoint((Personne) entrees.get(personnes.get(p)[0]-1));
                }
                if(personnes.get(p)[1] != -1) {
                    p.setSociete((Societe) entrees.get(personnes.get(p)[1]-1));
                }
            }
        }
    }

    // Utiliser pour les tests
    public void afficher() {
        for (Entree e : entrees) {
            System.out.println(e.toString(Presentation.ABREGE ,Sens.PRENOMS_NOM));
        }
    }
    // Utiliser pour les tests
    public int nombreEntrees() {
        return entrees.size();
    }

    public ArrayList<Entree> getEntrees() {
        return entrees;
    }


    public void ajoutEntree(Entree newEntree) {
        entrees.add(newEntree);
    }

    /*
       Pour les Sélections, je suis parti du principe que quand l'utilisateur sélectionne, il ne faut pas déselectionner
       les sélections actuelles et remplacer par la (les) nouvelle(s) mais en ajouter une et que dans le cas contraire, il
       faudrait passer par la méthode deselection
     */
    public void selection(String selec) {
        for (Entree comparaison: entrees) {
            if (comparaison.recherche(selec)) {
                selectionnees.add(comparaison);
            }
        }
    }

    public void selection(Entree selec) {
        for(Entree comparaison: entrees) {
            if (comparaison.equals(selec)) {
                selectionnees.add(comparaison);
                break;
            }
        }
    }

    public void selection(Entree[] selecs) {
        for(Entree comparaison: entrees) {
            if (Arrays.asList(selecs).contains(comparaison)) {
                selectionnees.add(comparaison);
            }
        }
    }

    public void deselection() {
        selectionnees.clear();
    }


    public ArrayList<Entree> recherche(String rech) {
        ArrayList<Entree> results = new ArrayList<Entree>();
        for (Entree comparaison: entrees) {
            if (comparaison.recherche(rech)) {
                results.add(comparaison);
            }
        }
        return results;
    }

    public void affichage(Ordre ordre, Presentation pres, Sens sens) {
        List<Entree> selections_copy = entrees;
        Collections.sort(selections_copy, new Comparator<Entree>() {
            @Override
            public int compare(final Entree entree1, final Entree entree2) {
                if (entree1 instanceof Personne) {
                    if (entree2 instanceof Personne) {
                        return (sens==Sens.NOM_PRENOMS) ? ((Personne) entree1).getNom().compareTo(((Personne) entree2).getNom()) : ((Personne) entree1).getPrenoms()[0].compareTo(((Personne) entree2).getPrenoms()[0]);
                    }
                    return (sens==Sens.NOM_PRENOMS) ? ((Personne) entree1).getNom().compareTo(((Societe) entree2).getNomSociete()) : ((Personne) entree1).getPrenoms()[0].compareTo(((Societe) entree2).getNomSociete());
                }
                if (entree2 instanceof Personne) {
                    return (sens==Sens.NOM_PRENOMS) ? ((Societe) entree1).getNomSociete().compareTo(((Personne) entree2).getNom()) : ((Societe) entree1).getNomSociete().compareTo(((Personne) entree2).getPrenoms()[0]);
                }
                return ((Societe) entree1).getNomSociete().compareTo(((Societe) entree2).getNomSociete());
            }
        });

        for (Entree entree: selections_copy) {
            if (entree instanceof Societe)  System.out.println(entree.toString(null,null));
            else {
                switch (pres) {
                    case SIMPLE:
                        System.out.println((sens==Sens.NOM_PRENOMS) ? ((Personne) entree).toString(Presentation.SIMPLE, Sens.NOM_PRENOMS) : ((Personne) entree).toString(Presentation.SIMPLE, Sens.PRENOMS_NOM));
                        break;
                    case COMPLET:
                        System.out.println((sens==Sens.NOM_PRENOMS) ? ((Personne) entree).toString(Presentation.COMPLET, Sens.NOM_PRENOMS) : ((Personne) entree).toString(Presentation.COMPLET, Sens.PRENOMS_NOM));
                        break;
                    default:
                        System.out.println((sens==Sens.NOM_PRENOMS) ? ((Personne) entree).toString(Presentation.ABREGE, Sens.NOM_PRENOMS) : ((Personne) entree).toString(Presentation.ABREGE, Sens.PRENOMS_NOM));
                        break;
                }
            }
        }
    }

    public void affichageSelection(Ordre ordre, Presentation pres, Sens sens) {
        ArrayList<Entree> selections_copy = (ArrayList<Entree>) selectionnees.clone();
        Collections.sort(selections_copy, new Comparator<Entree>() {
            @Override
            public int compare(final Entree entree1, final Entree entree2) {
                if (entree1 instanceof Personne) {
                    if (entree2 instanceof Personne) {
                        return (sens==Sens.NOM_PRENOMS) ? ((Personne) entree1).getNom().compareTo(((Personne) entree2).getNom()) : ((Personne) entree1).getPrenoms()[0].compareTo(((Personne) entree2).getPrenoms()[0]);
                    }
                    return (sens==Sens.NOM_PRENOMS) ? ((Personne) entree1).getNom().compareTo(((Societe) entree2).getNomSociete()) : ((Personne) entree1).getPrenoms()[0].compareTo(((Societe) entree2).getNomSociete());
                }
                if (entree2 instanceof Personne) {
                    return (sens==Sens.NOM_PRENOMS) ? ((Societe) entree1).getNomSociete().compareTo(((Personne) entree2).getNom()) : ((Societe) entree1).getNomSociete().compareTo(((Personne) entree2).getPrenoms()[0]);
                }
                return ((Societe) entree1).getNomSociete().compareTo(((Societe) entree2).getNomSociete());
            }
        });

        for (Entree entree: selections_copy) {
            if (entree instanceof Societe)  System.out.println(entree.toString(null,null));
            else {
                switch (pres) {
                    case SIMPLE:
                        System.out.println((sens==Sens.NOM_PRENOMS) ? ((Personne) entree).toString(Presentation.SIMPLE, Sens.NOM_PRENOMS) : ((Personne) entree).toString(Presentation.SIMPLE, Sens.PRENOMS_NOM));
                        break;
                    case COMPLET:
                        System.out.println((sens==Sens.NOM_PRENOMS) ? ((Personne) entree).toString(Presentation.COMPLET, Sens.NOM_PRENOMS) : ((Personne) entree).toString(Presentation.COMPLET, Sens.PRENOMS_NOM));
                        break;
                    default:
                        System.out.println((sens==Sens.NOM_PRENOMS) ? ((Personne) entree).toString(Presentation.ABREGE, Sens.NOM_PRENOMS) : ((Personne) entree).toString(Presentation.ABREGE, Sens.PRENOMS_NOM));
                        break;
                }
            }
        }
    }
}
